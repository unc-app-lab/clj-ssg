(ns edu.unc.applab.ssg.parse
  "This namespace is responsible for parsing information from the string
  contents of page and post files. These files tend to have some metadata at the
  top followed by content in markdown format. All functions in this namespace
  are pure."
  (:require [clojure.java.shell :as shell]
            [clojure.string :as string]
            [edu.unc.applab.ssg.util :as util]))

(defn split-metadata
  "Given a string with YAML metadata and markdown content, separate the two and
  return a vector containing both respectively."
  [input]
  (let [lines (string/split-lines input)
        separator-line? #(re-matches #"---\s*" %)
        metadata-lines (take-while (complement separator-line?) (rest lines))
        markdown-lines (drop (inc (count metadata-lines)) (rest lines))
        markdown (string/join (map #(str % "\n") markdown-lines))
        metadata (into {}
                       (for [line metadata-lines]
                         (let [[key & val-parts] (string/split line #":\s*")]
                           [(keyword key) (string/join ": " val-parts)])))]
    [metadata markdown]))

(defn markdown->html
  "Convert the given markdown to HTML. This is technically not a pure function
  since it shells out to pandoc, and system settings (like global pandoc
  configuration or pandoc version) could affect the returned value. However, in
  practice, this should generally return the same output given the same input so
  is deterministic."
  [markdown]
  (let [{:keys [exit out err]}
        (shell/sh "pandoc" "-fmarkdown" "-thtml4" :in markdown)]
    (if (not= 0 exit)
      (throw (RuntimeException.
               (format (str "shell/sh returned non-zero exit code (%d)."
                            "stdout is '%s' and stderr is '%s'.")
                       exit out err)))
      out)))

(defn extract-snippet
  "Given an html string, return the snippet, i.e. the part up to but not
  including the <!-- more --> marker."
  [html]
  (if-let [matches (re-find #"(?s)^(.*?)<!--\s*more\s*-->" html)]
    (second matches)
    (throw (RuntimeException. (str "Error: did not find <!--more--> marker")))))

(defn parse-page-data
  "Given a page content string read from disk, return a map with data about the
  page. For example, (parse-page-data (slurp \"pages/about.md\")) might return:
      {:title \"About the App Lab\"
       :body \"<p>Paragraph 1.</p>(rest of post in HTML format)\"
       :href \"/about/\"}"
  [contents]

  (let [[metadata markdown] (split-metadata contents)
        stem (:stem metadata)
        href (if (= "index" stem) "/" (format "/%s/" stem))
        body (markdown->html markdown)]
    (-> metadata
        (assoc :body body)
        (assoc :href href)
        (dissoc :stem))))

(defn parse-post-data
  "Given a post content string read from disk, return data about the post. For
  example, (parse-post-data (slurp \"posts/page_name.md\")) might return:
    {:date \"2018-11-16\"
     :author \"Jeff Terrell\"
     :title \"Best Post Evar!!1\"
     :tags [\"tag1\" \"tag2\"]
     :snippet \"<p>This is the best post evar.</p>\"
     :body \"<p>This is the best post evar.</p>(rest of post in HTML format)\"
     :slug \"best-post-evar\"
     :href \"/posts/2018/11/16/best-post-evar/\"}"
  [{:keys [enable-tags] :as config} contents]

  (let [[metadata markdown] (split-metadata contents)
        body (markdown->html markdown)
        snippet (extract-snippet body)
        metadata (if enable-tags
                   (update metadata :tags #(string/split % #",\s*"))
                   metadata)
        slug (util/slugify (:title metadata))
        [year month day] (string/split (:date metadata) #"-")
        href (format "/posts/%s/%s/%s/%s/" year month day slug)]

    (assoc metadata
           :snippet snippet
           :body body
           :slug slug
           :href href)))

(defn parse-pages
  "Given a sequence of page content strings read from disk, return a sequence of
  page maps (see parse-page-data for an example)."
  [page-files]
  (mapv parse-page-data page-files))

(defn parse-posts
  "Given a sequence of post content strings read from disk, return a sequence of
  post maps (see parse-post-data for an example) sorted newest-first."
  [config post-files]
  (->> post-files
       (mapv (partial parse-post-data config))
       (sort-by :date)
       reverse))
