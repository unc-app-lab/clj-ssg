# clj-ssg

A simple static site generator (SSG) written in Clojure.

## SSGs

An SSG takes content written in a simple language, usually Markdown, and
compiles it into a static site, which is a folder that can be served by a
simple web server.

## Rationale

This SSG was written because [the author](https://jeffterrell.tech/) suffers
from the [Lisp Curse](http://www.winestockwebdesign.com/Essays/Lisp_Curse.html)
and found it easier to write his own SSG instead of learning how to use
somebody else's SSG.  It serves his needs and not much more, so you might want
to use a more full-featured SSG instead of this one. That said, this one is
fairly lightweight, so it might be easier to learn than others.

## Usage

### Tools setup

First, install the [Clojure CLI](https://clojure.org/guides/getting_started) if
you haven't already.

Then, add the following to a `deps.edn` file in a new directory to setup a
dependency on and script alias for `clj-ssg`:

```
{:deps {clj-ssg/clj-ssg {:git/url "https://gitlab.com/unc-app-lab/clj-ssg"
                         :sha "<the SHA of the latest commit on master>"}}
 :aliases {:ssg {:main-opts ["-m" "edu.unc.applab.ssg.main"]}}}
```

For the `:sha` value, you can poke around in the [repo page on
GitLab](https://gitlab.com/unc-app-lab/clj-ssg) or do this in a bash-compatible
shell:

```
git clone https://gitlab.com/unc-app-lab/clj-ssg
cd clj-ssg
git rev-parse @
cd ..
rm -rf clj-ssg
```

Finally, install [pandoc](https://pandoc.org/).

### Configuration

This SSG supports minimal configuration. To configure it, add a `ssg-config.edn`
file at the root directory of your SSG content (i.e. the directory with
`deps.edn`). You can find the default configuration in the
`src/edu/unc/applab/ssg/config.clj` file of this repo. Copy the defaults into
your file and make whatever changes you need. When you compile the site, the
settings in your `ssg-config.edn` file will be respected.

### Content setup

`clj-ssg` expects certain types of content in certain places, so you need to
add this content to the directory containing the `deps.edn` you just created.

Make the required directories:

```
mkdir pages posts resources template
```

- `pages` contains static pages (e.g. an about page) in markdown format with
  YAML metadata (more on metadata below).
- `posts` contains blog posts in markdown format with YAML metadata.
- `resources` contains static resources that are simply copied into the
  destination `resources` directory.
- `template` contains 2 files, `header.html` and `footer.html`, which enclose
  the markup for other pages.

To be clear, this SSG assumes that you will write your own page template. If
you want inspiration, check out the one I created for the UNC App Lab: here's
[the repo](https://gitlab.com/unc-app-lab/website) and here's [the generated
site](https://applab.unc.edu/). Note that the template header refers to many
files in the `resources/` directory.

Pages and posts require [YAML
metadata](https://github.blog/2013-09-27-viewing-yaml-metadata-in-your-documents/)
to work. Specifically, posts require `title`, `author`, and `date` keys and
also accept a `tag` key with comma-separated values. Pages require `title` and
`stem` keys. The stem tells the SSG where to place the generated output; for
example, a stem of `about` will be placed in `about/index.html` in the
generated folder.

### Compile the site

To execute the SSG and compile the static site-in-a-folder, run:

```
clojure -M:ssg
```

If all was successful, your static site is available in the `dist/` directory.
You can [run a local web
server](https://developer.mozilla.org/en-US/docs/Learn/Common_questions/set_up_a_local_testing_server)
to see everything in your browser.

If anything is out of place or missing, the SSG should print a friendly error
message (without a stack trace) and refuse to affect the `dist/` directory. If
this doesn't happen, it can be considered a bug.

### Deployment

To deploy, just copy the `dist/` directory to a server somewhere.

I quite like [Netlify](https://www.netlify.com/) for this sort of thing. It
serves static sites for free, even if you use a custom domain name. And it can
integrate with your repo on GitHub/GitLab so that commits pushed to
`origin/master` are continuously deployed. I recommend letting Netlify build
the site for you, in which case you should add `dist/` to your `.gitignore`
file.

### Tests

To run the tests, which takes about 3 seconds:

```
clojure -M:test
```

## License

Copyright © 2020 UNC App Lab

Distributed under the MIT License.
