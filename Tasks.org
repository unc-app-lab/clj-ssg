* TODO add Open Graph Protocol metadata for page previews
- The [[http://ogp.me/][Open Graph protocol]] specifies a standard set of metadata to add to a page
  so that e.g. Slack will know how to display previews.
- This works via <meta> tags added to the <head> tag of a page.
- Every generated page should have ~og:type~ set to ~website~.
- Every generated page should have ~og:site_name~ set to the site-name config.
- Every generated page should have ~og:title~ set to the page title.
- Every generated page should have ~og:url~ set according to the site-host
  config and href.
- If a configuration mechanism exists and there is a default-preview-image file:
  - Set ~og:image~ to the href of the default-preview-image on every page.
  - Set ~og:image:type~ to the detected mime type, e.g. ~image/png~
  - Set ~og:image:height~ to the image height
  - Set ~og:image:width~ to the image width
- Ideally, the preview image would be settable via metadata on a given page or
  post.
- Test the metadata using http://developers.facebook.com/tools/debug/
- Also, you can send a Slack message to yourself to test the preview, but note
  that the URL is cached by Slack servers for at least 30 minutes. More info
  [[https://api.slack.com/robots][here]].
* TODO consider how to indicate the current page in a nav menu
- For example, if a user is viewing the about page, and the template's nav menu
  includes a link to the about page, there should be some way to style the link
  to the about page differently to indicate to the user that "this is where you
  are right now".
- Presumably this would happen by adding a class to the element in the nav menu.
- But how can the SSG do this for us? I really want something simple here,
  without heavy parsing of the template or an overly powerful logic mechanism in
  the template, and I'm not sure how simple I can make it. So this might take
  some hammock time.
* TODO check that nested files/dirs under resources/ are readable
