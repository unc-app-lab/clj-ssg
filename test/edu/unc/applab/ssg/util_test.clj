(ns edu.unc.applab.ssg.util-test
  (:require [clojure.java.io :as io]
            [clojure.test :refer [deftest is]]
            [edu.unc.applab.ssg.util :as sut])
  (:import java.io.File))

(deftest multi-group-by
  (let [maps [{:tags ['a 'b], :bar 1}
              {:tags ['b],    :bar 2}
              {:tags ['a],    :bar 3}]
        result (sut/multi-group-by :tags maps)
        expected {'a [{:tags ['a 'b], :bar 1}
                      {:tags ['a],    :bar 3}]
                  'b [{:tags ['a 'b], :bar 1}
                      {:tags ['b],    :bar 2}]}]
    (is (= result expected))))

(deftest slugify
  (is (= (sut/slugify "Of Mice and Men.") "mice-men")))

(defn- path [& elements]
  (.getPath (apply io/file elements)))

(deftest find-files
  ;; FIXME: brittle test, depends on the actual contents of the repo
  (letfn [(test-path [leaf]
            (path "test" "edu" "unc" "applab" "ssg" leaf))]
    (is (= (set (sut/find-files "test"))
           #{(test-path "pages_test.clj")
             (test-path "util_test.clj")
             (test-path "parse_test.clj")}))))

(deftest in-order-dirs
  (is (= [(path "test")
          (path "test" "edu")
          (path "test" "edu" "unc")
          (path "test" "edu" "unc" "applab")
          (path "test" "edu" "unc" "applab" "ssg")]
         (sut/in-order-dirs
           [(path "test" "edu" "unc" "applab" "ssg" "pages_test.clj")
            (path "test" "edu" "unc" "applab" "ssg" "util_test.clj")
            (path "test" "edu" "unc" "applab" "ssg" "parse_test.clj")]))))

(deftest in-order-dirs-with-custom-file-separator
  (is (= ["test"
          "test\\edu"
          "test\\edu\\unc"
          "test\\edu\\unc\\applab"
          "test\\edu\\unc\\applab\\ssg"]
         (sut/in-order-dirs ["test\\edu\\unc\\applab\\ssg\\pages_test.clj"
                             "test\\edu\\unc\\applab\\ssg\\util_test.clj"
                             "test\\edu\\unc\\applab\\ssg\\parse_test.clj"]
                            "\\"))))

(deftest needed-dist-dirs
  (is (= ["dist/foo" "dist/foo/bar"]
         (sut/needed-dist-dirs ["resources/foo/bar/baz.txt"]))))

(deftest needed-dist-dirs-with-custom-file-separator
  (is (= [(str "dist" (File/separator) "foo") (str "dist" (File/separator) "foo" (File/separator) "bar")]
         (sut/needed-dist-dirs ["resources\\foo\\bar\\baz.txt"] "\\"))))
