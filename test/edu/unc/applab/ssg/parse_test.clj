(ns edu.unc.applab.ssg.parse-test
  (:require [clojure.test :refer [deftest is]]
            [edu.unc.applab.ssg.config :as config]
            [edu.unc.applab.ssg.parse :as sut]))

(deftest split-metadata
  (let [input "---\ntitle: foo\n---\n# header\n"
        result (sut/split-metadata input)
        expected [{:title "foo"} "# header\n"]]
    (is (= result expected))))

(deftest markdown->html
  (let [input "paragraph\n\n<!--more-->\n\n- item 1\nitem 2\n"
        result (sut/markdown->html input)
        expected
        "<p>paragraph</p>\n<!--more-->\n<ul>\n<li>item 1 item 2</li>\n</ul>\n"]
    (is (= result expected))))

(deftest extract-snippet
  (let [input "<p>Teaser</p>\n<!--more-->\n<p>More...</p>\n"
        result (sut/extract-snippet input)
        expected "<p>Teaser</p>\n"]
    (is (= result expected))))

(deftest parse-page-data
  (let [input-file (str "---\n"
                        "title: About this site\n"
                        "stem: about\n"
                        "---\n\n"
                        "Blah\n")
        result (sut/parse-page-data input-file)
        expected {:title "About this site"
                  :body "<p>Blah</p>\n"
                  :href "/about/"}]
    (is (= result expected))))

(deftest parse-page-data-with-trailing-whitespace
  (let [input-file (str "---\n"
                        "title: About this site\n"
                        "stem: about\n"
                        "---   \n\n"
                        "Blah\n")
        result (sut/parse-page-data input-file)
        expected {:title "About this site"
                  :body "<p>Blah</p>\n"
                  :href "/about/"}]
    (is (= result expected))))

(deftest parse-post-data
  (let [input-file (str "---\n"
                        "title: Testing 1, 2, 3…\n"
                        "author: Jeff Terrell\n"
                        "tags: one\n"
                        "date: 2018-12-10\n"
                        "---\n\n"
                        "this is a post\n\n"
                        "<!--more-->\n\n"
                        "more content\n")
        result (sut/parse-post-data config/default input-file)
        expected
        {:title "Testing 1, 2, 3…"
         :author "Jeff Terrell"
         :tags ["one"]
         :date "2018-12-10"
         :snippet "<p>this is a post</p>\n"
         :body "<p>this is a post</p>\n<!--more-->\n<p>more content</p>\n"
         :slug "testing-1-2-3"
         :href "/posts/2018/12/10/testing-1-2-3/"}]
    (is (= result expected))))
