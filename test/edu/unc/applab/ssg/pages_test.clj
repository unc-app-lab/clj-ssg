(ns edu.unc.applab.ssg.pages-test
  (:require [clojure.test :refer [deftest is]]
            [edu.unc.applab.ssg.config :as config]
            [edu.unc.applab.ssg.markup :as markup]
            [edu.unc.applab.ssg.pages :as sut]
            [edu.unc.applab.ssg.parse :as parse]
            [edu.unc.applab.ssg.util :as util]))

(def posts [(parse/parse-post-data
              config/default
              (str "---\n"
                   "title: Testing 1, 2, 3…\n"
                   "author: Jeff Terrell\n"
                   "tags: one\n"
                   "date: 2018-12-10\n"
                   "---\n\n"
                   "this is a post\n\n"
                   "<!--more-->\n\n"
                   "more content\n"))])

(defmacro with-simple-headers [body]
  `(with-redefs [markup/header (constantly "<title>__TITLE__</title>\n")
                 markup/footer (constantly "<footer/>\n")]
     ~body))

(defn post-summary [title]
  (str (format "<title>%s</title>\n" title)
       (format "<h1>%s</h1>" title)
       "<div class=\"post-preview\">"
       "<h3 class=\"title\"><a href=\"/posts/2018/12/10/testing-1-2-3/\">"
       "Testing 1, 2, 3…</a></h3>"
       "<span class=\"date\">2018-12-10</span>"
       "<span class=\"author\"><a href=\"/author/jeff-terrell/\">"
       "Jeff Terrell</a></span>"
       "<div class=\"tags\"><a href=\"/tag/one/\">one</a></div>"
       "<span class=\"snippet\"><p>this is a post</p>\n"
       "</span><p><a href=\"/posts/2018/12/10/testing-1-2-3/\">"
       "Read more&hellip;</a></p></div><footer/>\n"))

(def index-ul
  (str
    "<ul>"
    "<li><a href=\"/posts/2018/12/10/testing-1-2-3/\">Testing 1, 2, 3…</a></li>"
    "</ul>"))

(deftest build-blog-index
  (let [pages (with-simple-headers (sut/build-blog-index config/default posts))
        {:keys [href html]} (first pages)
        expected (str "<title>UNC App Lab Blog</title>\n"
                      "<div class=\"post-preview\">"
                      "<h3 class=\"title\">"
                      "<a href=\"/posts/2018/12/10/testing-1-2-3/\">"
                      "Testing 1, 2, 3…</a></h3>"
                      "<span class=\"date\">2018-12-10</span>"
                      "<span class=\"author\">"
                      "<a href=\"/author/jeff-terrell/\">Jeff Terrell</a></span>"
                      "<div class=\"tags\"><a href=\"/tag/one/\">one</a></div>"
                      "<span class=\"snippet\"><p>this is a post</p>\n"
                      "</span><p><a href=\"/posts/2018/12/10/testing-1-2-3/\">"
                      "Read more&hellip;</a></p></div><footer/>\n")]
    (is (= href "/blog/"))
    (is (= html expected))))

(deftest author-pages
  (let [pages (with-simple-headers (sut/author-pages config/default
                                                     (group-by :author posts)))
        {:keys [href html]} (first pages)
        expected (post-summary "Posts by Jeff Terrell")]
    (is (= href "/author/jeff-terrell/"))
    (is (= html expected))))

(deftest author-index
  (let [page (with-simple-headers (sut/author-index (group-by :author posts)))
        expected (str
                   "<title>Author Index</title>\n"
                   "<h1><a href=\"/author/jeff-terrell/\">Jeff Terrell</a></h1>"
                   index-ul
                   "<footer/>\n")]
    (is (= (:html page) expected))))

(deftest tag-pages
  (let [pages (with-simple-headers
                (sut/tag-pages config/default
                               (util/multi-group-by :tags posts)))
        {:keys [href html]} (first pages)
        expected (post-summary "Posts tagged one")]
    (is (= href "/tag/one/"))
    (is (= html expected))))

(deftest tag-index
  (let [page (with-simple-headers
                        (sut/tag-index (util/multi-group-by :tags posts)))
        expected (str "<title>Tag Index</title>\n"
                      "<h1><a href=\"/tag/one/\">one</a></h1>"
                      index-ul
                      "<footer/>\n")]
    (is (= (:html page) expected))))

(deftest build-posts
  (let [pages (with-simple-headers (sut/build-posts config/default posts))
        {:keys [href html]} (first pages)
        expected (str "<title>Testing 1, 2, 3…</title>\n"
                      "<h1 class=\"title\">Testing 1, 2, 3…</h1>"
                      "<div class=\"metadata\">"
                      "<span class=\"date\">Published 2018-12-10</span>"
                      "<span class=\"author\">By "
                      "<a href=\"/author/jeff-terrell/\">"
                      "Jeff Terrell</a></span>"
                      "<div class=\"tags\">Tagged: "
                      "<a href=\"/tag/one/\">one</a></div></div>"
                      "<p>this is a post</p>\n"
                      "<!--more-->\n"
                      "<p>more content</p>\n"
                      "<footer/>\n")]
    (is (= href "/posts/2018/12/10/testing-1-2-3/"))
    (is (= html expected))))

(deftest build-pages
  (let [page-map {:title "About this site"
                  :href "/about/"
                  :body "...about this site..."}
        pages (with-simple-headers (sut/build-pages [page-map]))
        {:keys [href html]} (first pages)
        expected (str "<title>About this site</title>\n"
                      "<h1>About this site</h1>"
                      "...about this site..."
                      "<footer/>\n")]
    (is (= href "/about/"))
    (is (= html expected))))
